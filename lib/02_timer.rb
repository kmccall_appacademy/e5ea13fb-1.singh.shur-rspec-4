class Timer
  attr_reader :seconds
  attr_writer :seconds
  def initialize
    @seconds = 0
  end

  def time_string
    sec = @seconds % 60
    min = (@seconds / 60) % 60
    hr = @seconds / 3600
    "#{padded(hr.to_i)}:#{padded(min.to_i)}:#{padded(sec.to_i)}"
  end

  def padded(n)
    two_char = ''
    if n.zero?
      two_char = '00'
    elsif n < 10
      two_char = "0#{n}"
    elsif n < 100
      two_char = n.to_s
    end
    two_char
  end
end
