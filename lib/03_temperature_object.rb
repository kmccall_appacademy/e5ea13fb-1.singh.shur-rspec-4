class Temperature
  def initialize(temp)
    raise ArgumentError, 'missing kwarg :f or :c' unless temp[:f] || temp[:c]
    @mode = temp.keys[0]
    @degrees = temp.values[0]
  end

  def in_fahrenheit
    @mode == :f ? @degrees : @degrees * 9.0 / 5.0 + 32
  end

  def in_celsius
    @mode == :c ? @degrees : (@degrees - 32) * 5.0 / 9.0
  end

  def self.from_celsius(n)
    Temperature.new(c: n)
  end

  def self.from_fahrenheit(n)
    Temperature.new(f: n)
  end
end

class Celsius < Temperature
  def initialize(n)
    super(c: n)
  end
end

class Fahrenheit < Temperature
  def initialize(n)
    super(f: n)
  end
end
