class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(arg)
    if arg.class == Hash
      @entries[arg.keys[0]] = arg.values[0]
    elsif arg.class == String
      @entries[arg] = nil
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(string)
    keywords.include?(string)
  end

  def find(string)
    hits = keywords.grep(/#{string}/)
    @entries.select { |k, _| hits.include?(k) }
  end

  def printable
    out = ''
    keywords.each do |k|
      out << "[#{k}] \"#{@entries[k]}\"\n"
    end
    out.strip!
  end
end
