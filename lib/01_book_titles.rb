class Book
  attr_reader :title

  def initialize
    @articles = %w[a an the]
    @conjunctions = %w[and because but for if or]
    @prepositions = %w[in of with at from into to]
  end

  def title=(name)
    downcase_filters = @articles + @conjunctions + @prepositions
    @title = name.split(' ').map.with_index do |e, i|
      next e.capitalize if i.zero?
      downcase_filters.include?(e.downcase) ? e : e.capitalize
    end.join(' ')
  end

end
